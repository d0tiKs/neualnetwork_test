import numpy as np
import pickle
import datetime
import os 

#Sigmoid Function
def Sigmoid (x):
    return 1.0/(1.0 + np.exp(-x))

#Derivative of Sigmoid Function
def DerivativesSigmoid(x):
    return np.exp(-x)/pow((np.exp(-x) + 1), 2)

#Parameters
DefaultParameters = {
    'TrainingIterations': 8000,
    'LearningRate': 0.1,
    'RNGSeed' : 1,
    'RNGPrecision' : 2,
    'Sizes' : [2,2,2]
}

class Network():

    def __init__(self):
        pass

    def __init__(self, param = DefaultParameters, randominit = True):

        self.param = param
        np.random.seed(param['RNGSeed'])

        val = 0

        def initArray(x, y, randominit) :
            if randominit:
                res = np.random.uniform(size = (y,x))
            else:
               res = np.zeros((y,x))
            return res
        sizes = param['Sizes']

        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]
        self.activations = []

    def ForwardPropagation(self, data):
        activation = data[0]
        self.activations = [0] * len(self.sizes)
        self.activations[0] = activation 

        for i in range(len(self.weights)):

            weight = self.weights[i]
            bias = self.biases[i]

            dotResult = np.dot(weight, activation)
            activation = Sigmoid(dotResult + bias)

            self.activations[i+1] = activation

        return self.activations[-1:]


    def BackPropagation(self, data, learningRate):

        input = data[0]
        wantedOutput = data[1]
        output = self.activations[-1]

        ###

        err_out = (wantedOutput - output)
        slope_out =  DerivativesSigmoid(output)
        d_out = err_out * slope_out

        dw_out = np.dot(d_out, self.activations[-2].T)
        db_out = np.sum(d_out, axis=1, keepdims=True)

        self.weights[2] += dw_out * learningRate
        self.biases[2] += db_out * learningRate

        ###

        err_hidden2 = np.dot(self.weights[-1].T, err_out)
        slope_hidden2 =  DerivativesSigmoid(self.activations[-2])
        d_hidden2 = np.dot(slope_hidden2.T, err_hidden2)

        dw_hidden2 = np.dot(d_hidden2, self.activations[-3].T)
        db_hidden2 = np.sum(d_hidden2, axis=1, keepdims=True)

        self.weights[1] += dw_hidden2 * learningRate
        self.biases[1] += db_hidden2 * learningRate

        ###

        err_hidden = np.dot(self.weights[-2].T, err_hidden2)
        slope_hidden =  DerivativesSigmoid(self.activations[-3])
        d_hidden = np.dot(slope_hidden.T, err_hidden)

        dw_hidden = np.dot(d_hidden, input.T)
        db_hidden = np.sum(d_hidden, axis=1, keepdims=True)

        self.weights[0] += dw_hidden * learningRate
        self.biases[0] += db_hidden * learningRate

    def TrainNetwork(self, data, param = DefaultParameters):
        self.ForwardPropagation(data)
        self.BackPropagation(data, param['LearningRate'])
    
    def toPickle(self):

        data = {
            'param' : self.param,
            'sizes': self.sizes,
            'weights' : self.weights,
            'biases' : self.biases,
            'activations' : self.activations
        }

        return pickle.dumps(data, protocol = 0)

    @staticmethod
    def fromPickle(data):
        return pickle.load(data)


    def saveFile(self, name = ''):
        filename = name + str(datetime.datetime.now()).replace(':','_') + '.pckl'
        d = os.getcwd()
        with open(filename, 'ab') as f:
            f.write(self.toPickle())

        return filename

    @staticmethod
    def importFile(filename):

        data = {}

        with open(filename, 'rb') as f:
            data = Network.fromPickle(f)

        nn = Network()
        nn.param = data['param']
        nn.sizes = data['sizes']
        nn.weights = data['weights']
        nn.biases = data['biases']
        nn.activations = data['activations']

        return nn

