import os
import mnist
import numpy as np
import NeuralNetwork


Parameters = {
    'TrainingIterations': 1000,
    'LearningRate': 0.01,
    'RNGSeed' : 1,
    'RNGPrecision' : 2, # 2 digit after the coma
    'Sizes' : [784,16,16,2]
}

def loadData():
    mndata = mnist.MNIST('.\mnist')
    images, labels = mndata.load_training()

    data = []

    filtered_data = []
    max_digit = Parameters['Sizes'][-1] - 1

    def vectorizedValue(j):
        e = np.zeros((max_digit+1, 1))
        e[j] = 1.0
        return e

    for index, label in enumerate(labels):
        if (label < max_digit+1):
            vlabel = vectorizedValue(label)
            filtered_data.append([np.array(images[index]), vlabel])

    training_data = filtered_data[:1000]
    validation_data =  filtered_data[1001:2000]
    test_data =  filtered_data[2001:3000]
    
    return (training_data, validation_data, test_data)

def loadAndFormatData():
    tr_d, va_d, te_d = loadData()

    trainingInputs = [np.reshape(tr_d[x][0], (784, 1)) for x in range(len(tr_d))]
    trainingData = [(trainingInputs[i], tr_d[i][1]) for i in range(len(tr_d))]

    validationInputs = [np.reshape(va_d[x][0], (784, 1)) for x in range(len(va_d))]
    validationData = [(validationInputs[i], va_d[i][1]) for i in range(len(va_d))]

    testInputs = [np.reshape(te_d[x][0], (784, 1)) for x in range(len(te_d))]
    testData = [(testInputs[i], te_d[i][1]) for i in range(len(te_d))]

    return (trainingData, validationData, testData)


def main():
    trainingData, validationData, testData = loadAndFormatData()
    network = NeuralNetwork.Network(Parameters)

    for i in range(Parameters['TrainingIterations']):
        for j in range(len(trainingData)):
            network.TrainNetwork(trainingData[j], Parameters)

    fn = network.saveFile('./mnist/')

   # nn = NeuralNetwork.Network.importFile(fn)

    for x in range(10):
        digit = 0
        for i, v in enumerate(validationData[x][1]):
            if v == 1:
                digit = i
                pass

        value = network.ForwardPropagation(testData[x])
        print('Expected Answer:\n' + str(digit) + '\nNN Answer : \n' + str(np.round(value, 4) * 100))


if __name__ == '__main__':
    main()
